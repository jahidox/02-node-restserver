const { v4: uuidv4 } = require('uuid');
const path = require('path');

const uploadFile = (files, valid_extensions = ['png', 'jpg', 'jpeg', 'gif'], folder = '') => {
  return new Promise ( (resolve, reject) => {
    const {file} = files;
    const file_extension = file.name.split('.').slice(-1)[0];

    if (!valid_extensions.includes(file_extension)){
      return reject(`La extension ${file_extension} no es permitida - ${valid_extensions}`);
    }

    const temp_name = uuidv4() + '.' + file_extension;
    const uploadPath = path.join(__dirname, '../uploads/', folder, temp_name);

    file.mv(uploadPath, (err) => {
      if (err) {
        reject(err);
      }
      resolve(temp_name);
    });
  });
}

module.exports = {
  uploadFile
}