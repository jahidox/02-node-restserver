const { response } = require('express');
const bcryptjs = require('bcryptjs');
const User = require('../models/user.model');

const get_user = async (req, res= response) => {
  const { limit=5, since=0 } = req.query;

  // const users = await User.find({state:true})
  //     .skip(Number(since))
  //     .limit(Number(limit));

  // const total = await User.countDocuments({state:true})

  const [total, users] = await Promise.all([
    User.countDocuments({state:true}),
    User.find({state:true})
      .skip(Number(since))
      .limit(Number(limit))
  ]);

  res.json({
    msg: 'get',
    total,
    users
  });
};

const post_user = async (req, res) => {
  const { name, email, password, role } = req.body;
  const user = new User({name, email, password, role});

  const salt = bcryptjs.genSaltSync(10);
  user.password = bcryptjs.hashSync(password, salt);

  await user.save();

  res.status(201).json({
    msg: 'post',
    user
  });
};

const put_user = async (req, res= response) => {
  const { user_id } = req.params;
  const { _id, password, google, email, ...leftover} = req.body;

  if (password){
    const salt = bcryptjs.genSaltSync(10);
    leftover.password = bcryptjs.hashSync(password, salt);
  }

  const user = await User.findByIdAndUpdate(user_id, leftover)

  res.json({
    msg: 'put',
    user
  });
};

const patch_user = (req, res= response) => {
  res.json({
    msg: 'patch'
  });
};

const delete_user = async (req, res= response) => {
  const { user_id } = req.params;

  // const user = await User.findByIdAndDelete(user_id);
  const user = await User.findByIdAndUpdate(user_id, {state:false})

  res.json({
    msg: 'delete',
    user
  });
};

module.exports = {
  get_user,
  post_user,
  put_user,
  patch_user,
  delete_user
}
