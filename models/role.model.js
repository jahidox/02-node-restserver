const { Schema, model } = require('mongoose');

let role_schema = Schema({
  role: {
    type: String,
    required: [true, 'Rol obligatorio']
  }
});

module.exports = model('Role', role_schema);