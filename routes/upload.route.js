const { Router } = require('express');
const { check } = require('express-validator');
const { loadFile, updateImage, getImage, updateImageCloudinary } = require('../controllers/upload.controller');
const { validate_collections } = require('../helpers');
const { validate_fields, validateFile } = require('../middlewares');
const router = Router();

router.post('/', validateFile, loadFile);

router.put('/:collection/:id', [
  validateFile,
  check('id', 'El id no es de mongo').isMongoId(),
  check('collection').custom(c => validate_collections(c, ['users', 'products'])),
  validate_fields
], updateImageCloudinary);
// ], updateImage);

router.get('/:collection/:id', [
  check('id', 'El id no es de mongo').isMongoId(),
  check('collection').custom(c => validate_collections(c, ['users', 'products'])),
  validate_fields
], getImage);

module.exports = router;