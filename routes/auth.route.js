const { Router } = require('express');
const { check } = require('express-validator');
const { login, google_login } = require('../controllers/auth.controller');
const { validate_fields } = require('../middlewares/validate_fields');
const router = Router();

router.post('/login', [
  check('email', 'Correo es obligatorio').isEmail(),
  check('password', 'Contraseña es obligatoria').not().isEmpty(),
  validate_fields
], login);

router.post('/google-login', [
  check('id_token', 'El id_token es necesario').not().isEmpty(),
  validate_fields
], google_login);

module.exports = router;