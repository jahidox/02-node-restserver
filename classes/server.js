const express = require('express')
const cors = require('cors')
const fileUpload = require('express-fileupload');
const { db_connection } = require('../database/config');
class Server {
  constructor() {
    this.app = express();
    this.port = process.env.PORT;
    this.paths = {
      auth: '/api/auth',
      users: '/api/users',
      categories: '/api/categories',
      products: '/api/products',
      search: '/api/search',
      upload: '/api/upload',
    };

    //Connect to database
      this.connect_db();

    //Middlewares
    this.middlewares();

    //Routes
    this.routes();
  }

  connect_db() {
    db_connection();
  }

  middlewares() {
    this.app.use(cors());
    this.app.use(express.json()); //Body reading and parsing
    this.app.use( express.static('public'));
    this.app.use(fileUpload({
      useTempFiles : true,
      tempFileDir : '/tmp/',
      createParentPath: true
    }));
  }

  routes() {
    this.app.use(this.paths.auth, require('../routes/auth.route'));
    this.app.use(this.paths.users, require('../routes/user.route'));
    this.app.use(this.paths.categories, require('../routes/category.route'));
    this.app.use(this.paths.products, require('../routes/product.route'));
    this.app.use(this.paths.search, require('../routes/search.route'));
    this.app.use(this.paths.upload, require('../routes/upload.route'));
  }

  start() {
    this.app.listen(this.port, () => {
      console.log('Server running on port: ', this.port);
    });
  }
}

module.exports = Server;
